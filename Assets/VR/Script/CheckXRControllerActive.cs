using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class CheckXRControllerActive : MonoBehaviour
{
    public static bool CheckIfActivated(XRController controller, InputHelpers.Button activationTeleportButton, float activationTreshold = 0.1f)
    {
        InputHelpers.IsPressed(controller.inputDevice, activationTeleportButton, out bool isActivation, activationTreshold);
        return isActivation;
    }
}
