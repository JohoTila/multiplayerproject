using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class ButtonClickTeleport : MonoBehaviour
{
    [SerializeField] XRController leftTeleportRay;
    [SerializeField] XRController rightTeleportRay;
    [SerializeField] XRController leftRayHandUI;
    [SerializeField] XRController rightRayHandUI;
    [SerializeField] private InputHelpers.Button activationTeleportButton;
    [SerializeField] private float activationTreshold = 0.1f;
    bool actovTeleportL = false, actovTeleportR = false;
    [SerializeField] GameObject teleportObject;
    void Update()
    {
        if(teleportObject == null)
        {
            teleportObject = GameObject.Find("Teleport");
            teleportObject.SetActive(false);
            if (leftTeleportRay.GetComponent<XRInteractorLineVisual>().reticle == null)
            {
                leftTeleportRay.GetComponent<XRInteractorLineVisual>().reticle = teleportObject;
            }
            if (rightTeleportRay.GetComponent<XRInteractorLineVisual>().reticle == null)
            {
                rightTeleportRay.GetComponent<XRInteractorLineVisual>().reticle = teleportObject;
            }
        }

        if(leftTeleportRay && !actovTeleportR)
        {

            actovTeleportL = CheckXRControllerActive.CheckIfActivated(leftTeleportRay, activationTeleportButton, activationTreshold);
            leftTeleportRay.gameObject.SetActive(actovTeleportL);
            //if(actovTeleportL)
            //{
            //    rightRayHandUI.gameObject.SetActive(false);
            //    leftRayHandUI.gameObject.SetActive(false);
            //}
            //else
            //{
            //    rightRayHandUI.gameObject.SetActive(true);
            //    leftRayHandUI.gameObject.SetActive(true);
            //}
            
        }

        if (rightTeleportRay && !actovTeleportL)
        {
            actovTeleportR = CheckXRControllerActive.CheckIfActivated(rightTeleportRay, activationTeleportButton, activationTreshold);
            rightTeleportRay.gameObject.SetActive(actovTeleportR); 
            //if (actovTeleportR)
            //{
            //    rightRayHandUI.gameObject.SetActive(false);
            //    leftRayHandUI.gameObject.SetActive(false);
            //}
            //else
            //{
            //    rightRayHandUI.gameObject.SetActive(true);
            //    leftRayHandUI.gameObject.SetActive(true);
            //}
        }
    }
}
