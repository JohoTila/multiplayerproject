using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using UnityEngine.SceneManagement;

public class ConnectionOrCreateMap : NetworkBehaviour
{
   public void ConnectionMap()
    {
        NetworkManager.Singleton.StartClient();
        SceneManager.LoadScene(1);
    }

    public void CreateMap()
    {
        NetworkManager.Singleton.StartHost();
        SceneManager.LoadScene(1);   
    }
}
