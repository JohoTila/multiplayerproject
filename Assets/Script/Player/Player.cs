using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Player : NetworkBehaviour
{
    Ray ray;
    RaycastHit hit;

    [SerializeField] private float spee�Move = 0;
    private float _forwardTimeUot;
    public float forwardTimeUot;
    [SerializeField] private Vector3 moveDirection;
    [SerializeField] private Vector3 moveVelocity;
    [SerializeField] private CharacterController m_CharacterController;
    [SerializeField] private float gravity = 3;
    [SerializeField] private int damage;
    private void Update()
    {
        MovePlayer();
        if (Input.GetMouseButtonDown(0))
        {
            ShootPlayer();
        }
    }
    private void Start()
    {
        m_CharacterController = GetComponent<CharacterController>();
    }

    public void MovePlayer()
    {
        if(isLocalPlayer)
        {
            float moveX = Input.GetAxis("Horizontal");
            float moveZ = Input.GetAxis("Vertical");


            if (Input.GetAxis("Vertical") > 0.1 || Input.GetAxis("Vertical") < -0.1)
            {
                _forwardTimeUot += Time.deltaTime;
                if (_forwardTimeUot > forwardTimeUot)
                {
                    _forwardTimeUot = 0;
                }
            }

            if (Input.GetAxis("Horizontal") > 0.1 || Input.GetAxis("Horizontal") < -0.1)
            {
                _forwardTimeUot += Time.deltaTime;
                if (_forwardTimeUot > forwardTimeUot)
                {
                    _forwardTimeUot = 0;
                }
            }

            moveDirection = transform.right * moveX + transform.forward * moveZ;

            m_CharacterController.Move(moveDirection * spee�Move * Time.deltaTime);
            moveVelocity.y += gravity * Time.deltaTime;
            m_CharacterController.Move(moveVelocity * Time.deltaTime);
        }        
    }

    public void ShootPlayer()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if(Physics.Raycast(ray,out hit, 50))
        {
            if(hit.collider)
            {
                TargetDamage target = hit.collider.gameObject.GetComponent<TargetDamage>();
                if(target != null)
                {
                    target.TakeDamage(damage);
                }
            }
        }
    }
}
