using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveRotationCamera : MonoBehaviour
{
    public float sensetiveMouse; //����� ����
    [SerializeField] private float minMoveRotation; // �������� ��� ������� �� ����� �������� ����
    [SerializeField] private float maxMoveRotation; // �������� ��� ������� �� ����� �������� ����� 
    private float xRotation = 0f;
    [SerializeField] private Transform Player;

    [SerializeField] Image cursorImage;

    [SerializeField] private bool doubleClickMenu = false;
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    void Update()
    {
        float moveX = Input.GetAxis("Mouse X") * sensetiveMouse * Time.deltaTime;
            float moveY = Input.GetAxis("Mouse Y") * sensetiveMouse * Time.deltaTime;

            xRotation -= moveY;
            xRotation = Mathf.Clamp(xRotation, minMoveRotation, maxMoveRotation);
            transform.localRotation = Quaternion.Euler(xRotation, 0, 0);

            Player.Rotate(Vector3.up * moveX);

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                doubleClickMenu = !doubleClickMenu;
                if (doubleClickMenu)
                {
                    HideCursor();
                }
            }
            if (!doubleClickMenu)
            {
                ShowCursor();
            }        
    }

    public void HideCursor()
    {
        cursorImage.enabled = false;
    }

    public void ShowCursor()
    {
        cursorImage.enabled = true;
    }
}
