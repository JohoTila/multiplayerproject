using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.Messaging;


public class SpawnPersonah : NetworkBehaviour
{
    [SerializeField] private Transform player1;
    [SerializeField] private Transform player2;

    [SerializeField] List<GameObject> playerList = new List<GameObject>();
    [SerializeField] List<GameObject> pointSpawn = new List<GameObject>();
    void Start()
    {
        if(IsHost)
        {
            int randomIndexSpawnPoint = Random.Range(0, pointSpawn.Count);
            GameObject localPlayer = Instantiate(player1, pointSpawn[randomIndexSpawnPoint].transform.position, Quaternion.identity).gameObject;
            localPlayer.GetComponent<NetworkObject>().SpawnWithOwnership(NetworkManager.Singleton.LocalClientId);
            playerList.Add(localPlayer);
        }
        else
        {
            SpawnClientServerRpc();
        }
    }

    [ServerRpc]
    public void SpawnClientServerRpc()
    {
        int randomIndexSpawnPoint = Random.Range(0, pointSpawn.Count);
        GameObject localPlayer = Instantiate(player2, pointSpawn[randomIndexSpawnPoint].transform.position, Quaternion.identity).gameObject;
        localPlayer.GetComponent<NetworkObject>().SpawnWithOwnership(NetworkManager.Singleton.LocalClientId);
        playerList.Add(localPlayer);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
