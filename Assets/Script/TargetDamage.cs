using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetDamage : MonoBehaviour
{
    [SerializeField] private int _health = 100;

    public void TakeDamage(int amount)
    {
        _health -= amount;
        if(_health <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
